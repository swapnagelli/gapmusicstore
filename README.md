# GAP Music Store - Web Pages

This is a retail web site. This project is predominently to give a provision for end-user to buy/rent music 
and Developed by CIT GAP Team.

## Development team is
Role | Name
------------ | -------------
Project Manager | Swapna Gelli
PMO | Zaheer Mohammed
Developer | Veerender Sambaraju
Tester | Presila Israt


## Build & development

This project uses HTML as front-end and CSS for beautification of the site.

- [x] HTML5
- [X] CSS

## Procedure for integrating with Jenkins (to-do list)
1. Install Jenkins
2. Poll the CVS for any code check-ins
3. Create a workspace and clone the project to work space
4. Compile and build the web site
5. Deploy the project to Web Server

